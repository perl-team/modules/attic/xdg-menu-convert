
VENDOR=$(shell dpkg-vendor --query Vendor)
FILES=	usr/bin/xdg_menu 				\
	usr/bin/xdg_menu_su				\
	usr/bin/xdg_menu_update				\
	etc/profile.d/xdg-menu-convert			\
	etc/xdg/menus/$(VENDOR)-applications.menu

build: $(foreach f,$(FILES),builddir/$(f))

builddir/usr/bin builddir/etc/profile.d builddir/etc/xdg/menus:
	mkdir -p $@

builddir/usr/bin/xdg_menu_su: xdg_menu_su builddir/usr/bin
	cp $< $@

builddir/usr/bin/xdg_menu_update: update-menus builddir/usr/bin
	cp $< $@

builddir/usr/bin/xdg_menu: xdg_menu builddir/usr/bin
	sed -e's!arch-!$(VENDOR)-!g' $< > $@

builddir/etc/profile.d/xdg-menu-convert: debian/profile builddir/etc/profile.d
	cp $< $@

builddir/etc/xdg/menus/$(VENDOR)-applications.menu: arch-xdg-menu/arch-applications.menu builddir/etc/xdg/menus
	sed -e's!Arch!$(VENDOR)!g' $< > $@

test: build
	for i in xdg_menu xdg_menu_update; do perl -c builddir/usr/bin/$${i}; done

clean:
	rm -rf builddir
